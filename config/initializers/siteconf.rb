# encoding: utf-8

Siteconf.defaults[:site_name] = 'Viva La Guitar' # 站点名称，如: Rabel
Siteconf.defaults[:welcome_tip] = '欢迎访问<strong>Viva La Guitar</strong>' # 网站欢迎语, 支持html标签
Siteconf.defaults[:splash] = '<div class="hero-unit"><h1>Rabel</h1><p>新一代简洁吉他爱好者社区</p></div>'
Siteconf.defaults[:ga_id] = 'UA-51454774-3' # Google Analytics ID
Siteconf.defaults[:default_search_engine] = 'google' # 默认搜索引擎，可从下面的搜索引擎列表中选择
Siteconf.defaults[:seo_description] = 'Viva La Guitar - 吉他万岁 - 吉他爱好者社区' # SEO 描述
Siteconf.defaults[:short_intro] = '吉他爱好者社区' # 网站简短介绍, 显示在右侧边栏
Siteconf.defaults[:footer] = '<p>&copy; 2014 Viva La Guitar</p>'
Siteconf.defaults[:mobile_footer] = '&copy; 2014 Viva La Guitar'
Siteconf.defaults[:custom_css] = '' # 全局自定义CSS
Siteconf.defaults[:custom_js] = '' # 全局自定义JavaScript
Siteconf.defaults[:custom_head_tags] = '' # 自定义Head标签
Siteconf.defaults[:pagination_topics] = "25"
Siteconf.defaults[:pagination_comments] = "100"
Siteconf.defaults[:nav_position] = 'bottom'
Siteconf.defaults[:show_captcha] = 'off'
Siteconf.defaults[:custom_logo] = ''
Siteconf.defaults[:global_banner] = ''
Siteconf.defaults[:theme] = 'rabel'
Siteconf.defaults[:global_sidebar_block] = ''
Siteconf.defaults[:show_community_stats] = 'on'
Siteconf.defaults[:allow_markdown_in_topics] = 'off'
Siteconf.defaults[:allow_markdown_in_comments] = 'off'
Siteconf.defaults[:allow_markdown_in_pages] = 'on'
Siteconf.defaults[:topic_editable_period_str] = '5'
Siteconf.defaults[:reward_title] = '拨片'
Siteconf.defaults[:sticky_topics_heading] = '置顶话题'
Siteconf.defaults[:latest_topics_heading] = '最新讨论'
Siteconf.defaults[:topic_list_style] = 'complex'

